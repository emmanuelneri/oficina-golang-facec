package handler

import (
	"facec/blog/internal/db"
	"facec/blog/pkg"

	"github.com/gin-gonic/gin"
)

const paramPostId = "id"

type CommentHandler struct {
	commentRepository *db.CommentRepository
	paramsHandler     *ParamsHandler
}

func NewComment(commentRepository *db.CommentRepository, paramsHandler *ParamsHandler) *CommentHandler {
	return &CommentHandler{
		commentRepository: commentRepository,
		paramsHandler:     paramsHandler,
	}
}

func (co *CommentHandler) CreateComment(c *gin.Context) {
	post, status, responseError := co.paramsHandler.findPost(c, paramPostId)
	if responseError != nil {
		c.JSON(status, responseError)
		return
	}

	var requestComment *pkg.RequestComment
	if err := c.ShouldBindJSON(&requestComment); err != nil {
		c.JSON(400, pkg.ResponseError{
			Cause:   "invalid request body",
			Message: err.Error(),
		})

		return
	}

	comment, err := co.commentRepository.InsertComment(post.Id.String(), &pkg.Comment{
		Message: requestComment.Message,
		Person:  requestComment.Person,
	})

	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail on insert comment", err))
		return
	}

	c.JSON(201, comment)
}

func (co *CommentHandler) GetCommentsByPost(c *gin.Context) {
	post, status, responseError := co.paramsHandler.findPost(c, paramPostId)
	if responseError != nil {
		c.JSON(status, responseError)
		return
	}

	comments, err := co.commentRepository.GetComments(post.Id.String())
	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail on get comments by post", err))
		return
	}

	c.JSON(200, comments)
}

func (co *CommentHandler) GetCommentByCommentAndPost(c *gin.Context) {
	_, status, responseError := co.paramsHandler.findPost(c, paramPostId)
	if responseError != nil {
		c.JSON(status, responseError)
		return
	}

	commentId := c.Param("commentId")
	comments, err := co.commentRepository.GetCommentById(commentId)
	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail on get comment by id", err))
		return
	}

	c.JSON(200, comments)
}
