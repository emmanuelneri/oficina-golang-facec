package handler

import (
	"facec/blog/internal/db"
	"facec/blog/pkg"

	"github.com/gin-gonic/gin"
)

type UserHandler struct {
	userRepository *db.UserRepository
}

func NewUserHandler(userRepository *db.UserRepository) *UserHandler {
	return &UserHandler{
		userRepository: userRepository,
	}
}

func (u *UserHandler) CreateUser(c *gin.Context) {
	var user pkg.User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(400, pkg.NewResponseError("invalid user", err))
		return
	}

	u.userRepository.CreateUser(&user)

	c.JSON(201, pkg.ResponseUser{
		Login: user.Login,
	})
}
