package handler

import (
	"facec/blog/internal/db"
	"facec/blog/pkg"
	"fmt"

	"github.com/gin-gonic/gin"
)

type ParamsHandler struct {
	postRepository *db.PostRepository
}

func NewParamsHandler(postRepository *db.PostRepository) *ParamsHandler {
	return &ParamsHandler{postRepository: postRepository}
}

func (p *ParamsHandler) findPost(c *gin.Context, paramName string) (*pkg.Post, int, *pkg.ResponseError) {
	id := c.Param(paramName)
	post, err := p.postRepository.FindById(id)
	if err != nil {
		return nil, 500, &pkg.ResponseError{
			Cause:   "fail on find database",
			Message: err.Error(),
		}
	}

	if post == nil {
		return nil, 404, &pkg.ResponseError{
			Cause:   paramName + " not found",
			Message: fmt.Sprintf("id %s not found", id),
		}
	}

	return post, 0, nil
}
