package handler

import (
	"facec/blog/internal/commons"
	"facec/blog/internal/db"
	"facec/blog/pkg"

	"github.com/gin-gonic/gin"
)

type AuthenticationHandler struct {
	userRepository *db.UserRepository
}

func NewAuthentication(userRepository *db.UserRepository) *AuthenticationHandler {
	return &AuthenticationHandler{userRepository: userRepository}
}

func (a *AuthenticationHandler) Login(c *gin.Context) {
	var userRequest pkg.User
	if err := c.ShouldBindJSON(&userRequest); err != nil {
		c.JSON(400, pkg.NewResponseError("invalid user", err))
		return
	}

	user := a.userRepository.GetUser(userRequest.Login)
	if user == nil || userRequest.Password != user.Password {
		c.JSON(401, pkg.ResponseError{Cause: "user unauthorized"})
		return
	}

	token, err := commons.GenerateToken(user.Login)
	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail to generate token", err))
		return
	}

	c.JSON(200, gin.H{
		"token": token,
	})
}
