package handler

import (
	"facec/blog/internal/db"
	"facec/blog/pkg"
	"fmt"
	"io"
	"log"
	"time"

	"github.com/gin-gonic/gin"
)

const paramId = "id"

type PostHandler struct {
	postRepository *db.PostRepository
	paramsHandler  *ParamsHandler
}

func NewPost(postRepository *db.PostRepository, paramsHandler *ParamsHandler) *PostHandler {
	return &PostHandler{
		postRepository: postRepository,
		paramsHandler:  paramsHandler,
	}
}

func (p *PostHandler) GetPosts(c *gin.Context) {
	titleParam := c.Query("title")
	posts, err := p.postRepository.FindPosts(titleParam)
	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail on get post from database", err))
		return
	}

	c.JSON(200, posts)
}

func (p *PostHandler) CreatePost(c *gin.Context) {
	requestPost, responseError := parseBody(c)
	if responseError != nil {
		c.JSON(400, responseError)
		return
	}

	post := &pkg.Post{
		Title:    requestPost.Title,
		Body:     requestPost.Body,
		User:     requestPost.User,
		DateTime: time.Now(),
	}

	post, err := p.postRepository.InsertPost(post)
	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail on insert post", err))
		return
	}

	log.Println(fmt.Sprintf("post %s created", post))

	c.JSON(201, post)
}

func (p *PostHandler) UpdatePost(c *gin.Context) {
	post, status, responseError := p.paramsHandler.findPost(c, paramId)
	if responseError != nil {
		c.JSON(status, responseError)
		return
	}

	requestPost, responseError := parseBody(c)
	if responseError != nil {
		c.JSON(400, responseError)
		return
	}

	post.Title = requestPost.Title
	post.Body = requestPost.Body
	post.User = requestPost.User
	post.DateTime = time.Now()

	err := p.postRepository.Update(post)
	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail on update post", err))
		return
	}

	c.JSON(200, post)
}

func (p *PostHandler) PartialUpdatePost(c *gin.Context) {
	existingPost, status, responseError := p.paramsHandler.findPost(c, paramId)
	if responseError != nil {
		c.JSON(status, responseError)
		return
	}

	var partialRequest pkg.PartialRequestPost
	if err := c.ShouldBindJSON(&partialRequest); err != nil {
		if err == io.EOF {
			log.Println("[WARN] -  empty json", err)
			c.JSON(400, pkg.NewResponseError("empty json", err))
			return
		}

		log.Println("[WARN] -  invalid json", err)
		c.JSON(400, pkg.NewResponseError("invalid json", err))
		return
	}

	updatePost := &pkg.Post{
		Id: existingPost.Id,
	}

	if partialRequest.Title != nil {
		updatePost.Title = *partialRequest.Title
	}

	if partialRequest.Body != nil {
		updatePost.Body = *partialRequest.Body
	}

	if partialRequest.User != nil {
		updatePost.User = *partialRequest.User
	}

	err := p.postRepository.PartialUpdate(updatePost)
	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail on partial update", err))
		return
	}

	c.JSON(204, "")
}

func (p *PostHandler) DeletePost(c *gin.Context) {
	post, status, responseError := p.paramsHandler.findPost(c, paramId)
	if responseError != nil {
		c.JSON(status, responseError)
		return
	}

	err := p.postRepository.DeletePost(post.Id.String())
	if err != nil {
		c.JSON(500, pkg.NewResponseError("fail on delete", err))
		return
	}

	c.JSON(204, "")
}

func (p *PostHandler) GetPost(c *gin.Context) {
	post, status, responseError := p.paramsHandler.findPost(c, paramId)
	if responseError != nil {
		c.JSON(status, responseError)
		return
	}

	c.JSON(200, post)
}

func parseBody(c *gin.Context) (*pkg.RequestPost, *pkg.ResponseError) {
	var requestPost pkg.RequestPost

	if err := c.ShouldBindJSON(&requestPost); err != nil {
		if err == io.EOF {
			log.Println("[WARN] -  empty json", err)
			return nil, pkg.NewResponseError("empty json", err)
		}

		log.Println("[WARN] -  invalid json", err)
		return nil, pkg.NewResponseError("invalid json", err)
	}

	return &requestPost, nil
}
