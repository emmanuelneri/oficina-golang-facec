package http

import (
	"facec/blog/internal/container"
	"facec/blog/internal/handler"
	"facec/blog/internal/middleware"

	"github.com/gin-gonic/gin"
)

func StartServer(container *container.Container) error {
	engine := gin.Default()
	gin.EnableJsonDecoderDisallowUnknownFields()

	authentication := handler.NewAuthentication(&container.UserRepository)
	paramsHandler := handler.NewParamsHandler(container.PostRepository)
	postHandler := handler.NewPost(container.PostRepository, paramsHandler)
	commentHandler := handler.NewComment(container.CommentRepository, paramsHandler)
	userHandler := handler.NewUserHandler(&container.UserRepository)

	engine.GET("/health", handler.HealthCheck)
	engine.POST("/users", userHandler.CreateUser)
	engine.POST("/login", authentication.Login)

	posts := engine.Group("/posts")
	posts.Use(middleware.TokenValidate())
	posts.GET("/", postHandler.GetPosts)
	posts.POST("/", postHandler.CreatePost)
	posts.GET("/:id", postHandler.GetPost)
	posts.PUT("/:id", postHandler.UpdatePost)
	posts.PATCH("/:id", postHandler.PartialUpdatePost)
	posts.DELETE("/:id", postHandler.DeletePost)
	posts.POST("/:id/comments", commentHandler.CreateComment)
	posts.GET("/:id/comments", commentHandler.GetCommentsByPost)
	posts.GET("/:id/comments/:commentId", commentHandler.GetCommentByCommentAndPost)

	return engine.Run()
}
