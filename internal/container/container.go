package container

import (
	"facec/blog/internal/db"
)

type Container struct {
	PostRepository    *db.PostRepository
	CommentRepository *db.CommentRepository
	UserRepository    db.UserRepository
}

func NewContainer() *Container {
	return &Container{}
}

func (c *Container) Start() error {
	pool, err := db.StartPool()
	if err != nil {
		return err
	}

	c.PostRepository = db.NewPostRepository(pool)
	c.CommentRepository = db.NewCommentRepository(pool)
	c.UserRepository = db.UserRepository{}

	return nil
}
