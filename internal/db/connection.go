package db

import (
	"context"

	"github.com/jackc/pgx/v4/pgxpool"
)

func StartPool() (*pgxpool.Pool, error) {
	config, err := pgxpool.ParseConfig("user=postgres password=postgres host=localhost port=5432 dbname=apigo")
	if err != nil {
		return nil, err
	}

	config.MinConns = 2
	config.MinConns = 10

	return pgxpool.ConnectConfig(context.Background(), config)
}
