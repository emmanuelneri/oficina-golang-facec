package db

import (
	"context"
	"facec/blog/pkg"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type CommentRepository struct {
	dbPool *pgxpool.Pool
}

func NewCommentRepository(pool *pgxpool.Pool) *CommentRepository {
	return &CommentRepository{dbPool: pool}
}

func (c *CommentRepository) InsertComment(postId string, comment *pkg.Comment) (*pkg.Comment, error) {
	var id uuid.UUID
	sql := "insert into comment(post_id, person, message, date_time) values ($1, $2, $3, $4) returning id"
	err := c.dbPool.QueryRow(context.Background(), sql, postId, comment.Person, comment.Message, time.Now()).Scan(&id)
	if err != nil {
		return nil, err
	}

	comment.Id = id

	return comment, nil
}

func (c *CommentRepository) GetComments(postId string) ([]*pkg.Comment, error) {
	sql := "select id, person, message from comment where post_id = $1"

	rows, err := c.dbPool.Query(context.Background(), sql, postId)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	comments := make([]*pkg.Comment, 0)
	for rows.Next() {
		var id uuid.UUID
		var person string
		var message string

		rows.Scan(&id, &person, &message)
		comments = append(comments, &pkg.Comment{
			Id:      id,
			Person:  person,
			Message: message,
		})
	}

	return comments, nil
}

func (c *CommentRepository) GetCommentById(commentId string) (*pkg.Comment, error) {
	sql := "select id, person, message from comment where id = $1"

	var id uuid.UUID
	var person string
	var message string
	err := c.dbPool.QueryRow(context.Background(), sql, commentId).Scan(&id, &person, &message)
	if err != nil {
		return nil, err
	}

	return &pkg.Comment{
		Id:      id,
		Person:  person,
		Message: message,
	}, nil
}
