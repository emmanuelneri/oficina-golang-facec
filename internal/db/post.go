package db

import (
	"context"
	"facec/blog/pkg"
	"fmt"
	"time"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4/pgxpool"
)

type PostRepository struct {
	dbPool *pgxpool.Pool
}

func NewPostRepository(pool *pgxpool.Pool) *PostRepository {
	return &PostRepository{dbPool: pool}
}

func (p *PostRepository) FindPosts(titleParam string) ([]*pkg.Post, error) {
	posts := make([]*pkg.Post, 0)

	sql := "select id, title, body, date_time, \"user\" from post"
	param := make([]interface{}, 0)
	if len(titleParam) > 0 {
		sql = sql + " where title = $1"
		param = append(param, titleParam)
	}

	rows, err := p.dbPool.Query(context.Background(), sql)
	if err != nil {
		return nil, err
	}

	defer rows.Close()
	for rows.Next() {
		var id uuid.UUID
		var title string
		var body string
		var dateTime time.Time
		var user string

		rows.Scan(&id, &title, &body, &dateTime, &user)

		posts = append(posts, &pkg.Post{
			Id:       id,
			Title:    title,
			Body:     body,
			DateTime: dateTime,
			User:     user,
		})
	}

	return posts, nil
}

func (p *PostRepository) InsertPost(post *pkg.Post) (*pkg.Post, error) {
	sql := "insert into post (title, body, date_time, \"user\") values ($1, $2, $3, $4) returning id"

	var id uuid.UUID
	err := p.dbPool.QueryRow(context.Background(), sql,
		post.Title, post.Body, post.DateTime, post.User).Scan(&id)

	if err != nil {
		return nil, err
	}

	post.Id = id

	return post, nil
}

func (p *PostRepository) DeletePost(id string) error {
	sql := "delete from post where id = $1"
	_, err := p.dbPool.Exec(context.Background(), sql, id)
	return err
}

func (p *PostRepository) FindById(idParam string) (*pkg.Post, error) {
	sql := "select id, title, body, date_time,\"user\" from post where id = $1"

	var id uuid.UUID
	var title string
	var body string
	var dateTime time.Time
	var user string

	err := p.dbPool.QueryRow(context.Background(), sql, idParam).Scan(&id, &title, &body, &dateTime, &user)
	if err != nil {
		return nil, err
	}

	return &pkg.Post{
		Id:       id,
		Title:    title,
		Body:     body,
		DateTime: dateTime,
		User:     user,
	}, nil
}

func (p *PostRepository) Update(post *pkg.Post) error {
	sql := "update post set title = $1, body = $2, date_time = $3, \"user\" = $4 where id = $5"
	_, err := p.dbPool.Exec(context.Background(), sql, post.Title, post.Body, post.DateTime, post.User, post.Id)
	return err
}

func (p *PostRepository) PartialUpdate(post *pkg.Post) error {
	sql := "update post set"

	params := make([]interface{}, 0)

	setParamIfExistis("title", post.Title, &params, &sql)
	setParamIfExistis("body", post.Body, &params, &sql)
	setParamIfExistis("\"user\"", post.User, &params, &sql)

	_, err := p.dbPool.Exec(context.Background(), sql, params...)
	return err
}

func setParamIfExistis(paramName string, paramValue string, params *[]interface{}, sql *string) {
	if len(paramValue) > 0 {
		*sql = *sql + fmt.Sprintf(" %s = $%d", paramName, len(*params)+1)
		*params = append(*params, paramValue)
	}
}
