package db

import "facec/blog/pkg"

// User doesn`t use database, it`s a example with memory storage
var userMap = make(map[string]*pkg.User, 0)

type UserRepository struct {
}

func (UserRepository) CreateUser(user *pkg.User) {
	userMap[user.Login] = user
}

func (UserRepository) GetUser(login string) *pkg.User {
	return userMap[login]
}
