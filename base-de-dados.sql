CREATE TABLE "user"
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    login VARCHAR(200) NOT NULL,
    password VARCHAR(72) NOT NULL,
    CONSTRAINT user_uk UNIQUE(login)
);


CREATE TABLE post
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    title VARCHAR(200) NOT NULL,
    body text NOT NULL,
    date_time timestamp NOT NULL,
    "user" VARCHAR(200) NOT NULL
);

CREATE TABLE comment
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    post_id UUID NOT NULL,
    person VARCHAR(200) NOT NULL,
    message VARCHAR(500) NOT NULL,
    date_time timestamp NOT NULL,
    CONSTRAINT comment_post_id_fk FOREIGN KEY (post_id) REFERENCES post(id)
);