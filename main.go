package main

import (
	"facec/blog/internal/container"
	"facec/blog/internal/http"
	"log"
)

func main() {
	log.Println("iniciando...")

	co := container.NewContainer()
	if err := co.Start(); err != nil {
		log.Fatal(err)
	}

	err := http.StartServer(co)
	log.Fatalln(err)
}
